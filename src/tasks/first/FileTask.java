package tasks.first;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FileTask {

  public static void main(String[] args) {
    ConsoleHelper.writeMessage("Enter full path to text file.");
    try {
      // read file
      String fileAbsolutePath = ConsoleHelper.readString();
      StringBuilder sb = new StringBuilder();
      try (BufferedReader br = new BufferedReader(new FileReader(fileAbsolutePath))) {
        String line;
        while ((line = br.readLine()) != null) {
          sb.append(line);
        }
      }
      // parse file
      List<String> stringNumbers = Arrays.asList(sb.toString().split(","));
      // sort numbers
      List<Integer> integers = stringNumbers.stream().map(Integer::parseInt).collect(Collectors.toList());
      Collections.sort(integers, new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
          return o1 - o2;
        }
      });
      ConsoleHelper.writeMessage(integers.toString());
      Collections.sort(integers, new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
          return o2 - o1;
        }
      });
      ConsoleHelper.writeMessage(integers.toString());
    } catch (IOException e) {
      ConsoleHelper.writeMessage("Oops, smth wrong!");
    }
  }
}
