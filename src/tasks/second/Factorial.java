package tasks.second;

public class Factorial {

  private int number;

  public Factorial(int number) {
    this.number = number;
  }

  public static void main(String[] args) {
    Factorial factorial = new Factorial(20);
    System.out.println(factorial);
  }

  private long countFactorial(int number) {
    if(number == 1){
      return 1;
    }
    return number * (countFactorial(number - 1));
  }

  @Override
  public String toString() {
    return "factorial of " + number + " = " + countFactorial(number);
  }
}
